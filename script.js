var currentSize;
var currentNumber;

function onPageLoad() {
	console.log("Page loaded.");
	document.getElementById("sizeField").addEventListener("change", textFieldChanged);
	document.getElementById("inputField").addEventListener("change", textFieldChanged);
	textFieldChanged();
}

function textFieldChanged() {
	
	var newSize = document.getElementById("sizeField").value;
	if (newSize != currentSize) currentSize = newSize;
	
	var svgMain = document.getElementById("svgMain");
	svgMain.style.width = svgMain.style.height = currentSize;
	
	var newNumber = document.getElementById("inputField").value;
	if (newNumber != currentNumber) currentNumber = newNumber;
	
	svgMain.innerHTML = "";
	var circleRadius = currentSize / 6;
	
	var circleMiddle = document.createElement("CIRCLE");
	circleMiddle.style.x = currentSize / 2;
	circleMiddle.style.y = currentSize / 2;
	circleMiddle.style.r = circleRadius;
	svgMain.appendChild(circleMiddle);
	/*ctx.beginPath();
	ctx.arc(currentSize / 2, currentSize / 2, circleRadius, 0, 2 * Math.PI);
	ctx.fill();
	ctx.stroke();
	
	ctx.beginPath();
	ctx.arc(currentSize / 2, 0, circleRadius, 0, Math.PI);
	ctx.fill();
	ctx.stroke();
	
	ctx.beginPath();
	ctx.arc(currentSize, currentSize / 2, circleRadius, Math.PI / 2, 3 * Math.PI / 2);
	ctx.fill();
	ctx.stroke();
	
	ctx.beginPath();
	ctx.arc(currentSize / 2, currentSize, circleRadius, Math.PI, 0);
	ctx.fill();
	ctx.stroke();
	
	ctx.beginPath();
	ctx.arc(0, currentSize / 2, circleRadius, 3 * Math.PI / 2, Math.PI / 2);
	ctx.fill();
	ctx.stroke();*/
}